1. luaconf.h
	Added scripts directory to the package path.

Lua 5.1.1
+++++++++
1. loadlib.c
	setpath is commented and unusable. getenv is failing on XDK NDEBUG build

2. loslib.c
	os_execute is commented and unusable. system is failing on XDK NDEBUG build
	os_getenv is commented and unusable. getenv is failing on XDK NDEBUG build
3. lua.c
	removed from make. 

4. luaconf.h
	a.	#pragma warning( disable : 4244 ) added to remove warning about possible when converting from "type1" to "type2"
	b.	LUA_NUMBER changed from double to float
	c.	LUA_NUMBER_SCAN changed from "%lf" to "%f"
	
5. lauxlib.c
	a.	// AKPATCHEND added to the ak_lua_realloc and ak_lua_free function declaration block
	b.	// AKPATCHEND added to l_alloc function definition
	
6. lmathlib.c
	An 'f' added to PI and RADIANS_PER_DEGREE definitions to make it float numbers

/////////////////////////////////////////////////////////////////////////////////////////////////

Lua 5.1.2
+++++++++
Added on April 04, 2007

1. loadlib.h
		Added #define for getenv( ). getenv( ) is failing on XDK NDEBUG build
		
2. loslib.c
		Added #define for getenv( ) and system( ). getenv( ) and system( ) is failing on XDK NDEBUG build

3. lua.c
		removed from make. 

4. luaconf.h
		a.	#pragma warning( disable : 4244 ) added to remove warning about possible data loss when converting from "type1" to "type2"
		b.	LUA_NUMBER changed from double to float and removed #define LUA_NUMBER_DOUBLE
		c.	LUA_NUMBER_SCAN changed from "%lf" to "%f"
	
5. lauxlib.c
	a.	// AKPATCHEND added to the ak_lua_realloc and ak_lua_free function declaration block
	b.	// AKPATCHEND added to l_alloc function definition
	
6. lmathlib.c
		An 'f' added to PI and RADIANS_PER_DEGREE definitions to make it float numbers.

++++++++++++++++++++
Monday, May 21, 2007

1. Removed previous modifications to lauxlib.c. Using lua_newstate and providing our allocator, will remove our 'hack' to the lua code.
